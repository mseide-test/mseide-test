program msehash_test;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
{$ifdef mswindows}{$apptype console}{$endif}
uses
 {$ifdef FPC}{$ifdef unix}cthreads,cwstring,{$endif}{$endif}
 sysutils,msehash,msetypes,msenoise,msearrayutils;
 
var
 t: tintegerhashdatalist;

procedure checkvalue(const actual: integer; const expected: integer;
                         const errorcode: integer = 1);
begin
 if actual <> expected then begin
  halt(errorcode);
 end;
end;

procedure checkorder(const values: array of integer; const errorcode: integer);
var
 po1: pintegerdataty;
 i1: int32;
begin
 checkvalue(t.count,length(values),errorcode);
 for i1:= 0 to high(values) do begin
  po1:= t.find(values[i1]);
  if po1 = nil then begin
   halt(errorcode);
  end;
  dec(pointer(po1),sizeof(integer));
  checkvalue(po1^.key,values[i1],errorcode);
 end;
 po1:= t.first();
 for i1:= 0 to high(values) do begin
  checkvalue(po1^.key,values[i1],errorcode);
  po1:= t.next();
 end;
 for i1:= 0 to high(values) do begin
  checkvalue(po1^.key,values[i1],errorcode);
  po1:= t.next();
 end;
 po1:= t.last();
 for i1:= high(values) downto 0 do begin
  checkvalue(po1^.key,values[i1],errorcode);
  po1:= t.prev();
 end;
 for i1:= high(values) downto 0 do begin
  checkvalue(po1^.key,values[i1],errorcode);
  po1:= t.prev();
 end;
end;

var
 ar1,ar2,ar3: integerarty;
 i1,i2: int32;
 puint1: ptruint;
 
begin
 t:= tintegerhashdatalist.create(0);
 checkorder([],1);
 t.addunique(1);
 checkorder([1],2);
 t.addunique(2);
 checkorder([1,2],3);
 t.addunique(2);
 checkorder([1,2],4);
 t.addunique(3);
 checkorder([1,2,3],5);
 t.add(4);
 checkorder([1,2,3,4],6);
 t.add(5);
 checkorder([1,2,3,4,5],7);
 t.add(6);
 checkorder([1,2,3,4,5,6],8);
 t.add(7);
 checkorder([1,2,3,4,5,6,7],9);
 t.add(8);
 checkorder([1,2,3,4,5,6,7,8],10);
 t.add(9);
 checkorder([1,2,3,4,5,6,7,8,9],11);
 t.addunique(9);
 checkorder([1,2,3,4,5,6,7,8,9],12);
 t.addunique(0);
 checkorder([1,2,3,4,5,6,7,8,9,0],13);

 t.delete(3);
 checkorder([1,2,4,5,6,7,8,9,0],14);
 t.delete(1);
 checkorder([2,4,5,6,7,8,9,0],15);
 t.delete(0);
 checkorder([2,4,5,6,7,8,9],16);
 t.delete(3);
 checkorder([2,4,5,6,7,8,9],17);
 
 t.addunique(0);
 checkorder([2,4,5,6,7,8,9,0],18);
 
 t.addunique(1);
 checkorder([2,4,5,6,7,8,9,0,1],19);
 t.addunique(3);
 checkorder([2,4,5,6,7,8,9,0,1,3],20);
 t.addunique(10);
 
 checkorder([2,4,5,6,7,8,9,0,1,3,10],21);
 
 t.capacity:= t.count;
 checkorder([2,4,5,6,7,8,9,0,1,3,10],22);

 t.delete(5);
 checkorder([2,4,6,7,8,9,0,1,3,10],23);
 t.delete(6);
 checkorder([2,4,7,8,9,0,1,3,10],24);
 t.delete(10);
 checkorder([2,4,7,8,9,0,1,3],25);
 t.delete(2);
 checkorder([4,7,8,9,0,1,3],26);

 t.capacity:= t.count;
 checkorder([4,7,8,9,0,1,3],27);
 t.clear();
 checkorder([],28);
 setlength(ar1,37829);
 for i1:= 0 to high(ar1) do begin
  ar1[i1]:= i1;
  t.add(i1);
 end;
 checkorder(ar1,29);

 t.mark(puint1);
 setlength(ar2,49825);
 for i1:= 0 to high(ar2) do begin
  i2:= mwcnoise();
  ar2[i1]:= i2;
  t.add(i2);
 end;
 ar3:= copy(ar1);
 stackarray(ar2,ar3);
 checkorder(ar3,30); 
 t.release(puint1);
 checkorder(ar1,31);

 t.clear();
 checkorder([],32);
 t.capacity:= 100000;
 for i1:= 0 to high(ar1) do begin
  t.add(i1);
 end;
 checkorder(ar1,33);
 t.destroy();
end.
